﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku.ConsoleClient
{
    /*
    ---------
    ---------
    ---------
    ---------
    ---------
    ---------
    ---------
    ---------
    ---------

    -----6--1
    -87--5---
    -------82
    3--1-4--6
    --43-8---
    --95--43-
    --6------
    9----125-
    2-37-9---

    53--7----
    6--195---
    -98----6-
    8---6---3
    4--8-3--1
    7---2---6
    -6----28-
    9-1537--4
    ---419--5
    ----8--79
    */
    public class Program
    {
        private const int fieldSize = 9;
        private static char[,] field = new char[fieldSize, fieldSize];

        static void Main()
        {
            ReadSudoku();
            try
            {
                SolveSudoku();
            }
            catch (Exception)
            { }
            PrintSudoku();
        }

        private static void PrintSudoku()
        {
            StringBuilder result = new StringBuilder(fieldSize * fieldSize);
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    result.Append(field[i, j]);
                }
                result.AppendLine();
            }
            Console.WriteLine(result.ToString().Trim());
        }

        private static void SolveSudoku(int row = 0, int col = 0)
        {
            if (row > 8)
            {
                throw new Exception("Solve");
            }
            if (field[row, col] == '-')
            {
                for (int i = 1; i <= fieldSize; i++)
                {
                    if (CheckRow(row, i) &&
                        CheckCol(col, i) &&
                        CheckSqare(row, col, i))
                    {
                        field[row, col] = (char)(i + '0');
                        SolveSudoku(NextRow(row, col), NextCol(col));
                        field[row, col] = '-';
                    }
                }
            }
            else
            {
                SolveSudoku(NextRow(row, col), NextCol(col));
            }
        }

        private static int NextCol(int col)
        {
            if (col >= 8)
            {
                return 0;
            }
            return col + 1;
        }

        private static int NextRow(int row, int col)
        {
            if (col >= 8)
            {
                return row + 1;
            }
            return row;
        }

        private static bool CheckSqare(int row, int col, int candidate)
        {
            int minRow = row < 3 ? 0 : row < 6 ? 3 : 6;
            int minCol = col < 3 ? 0 : col < 6 ? 3 : 6;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (field[minRow + i, minCol + j] == (char)(candidate + '0'))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static bool CheckCol(int col, int candidate)
        {
            for (int i = 0; i < fieldSize; i++)
            {
                if (field[i, col] == (char)(candidate + '0'))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool CheckRow(int row, int candidate)
        {
            for (int i = 0; i < fieldSize; i++)
            {
                if (field[row, i] == (char)(candidate + '0'))
                {
                    return false;
                }
            }
            return true;
        }

        private static void ReadSudoku()
        {
            for (int i = 0; i < fieldSize; i++)
            {
                string line = Console.ReadLine();
                for (int j = 0; j < fieldSize; j++)
                {
                    field[i, j] = line[j];
                }
            }
        }
    }
}
